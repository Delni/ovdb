import { RootStoreType } from 'src/store';
import { CarState } from 'src/store/car/car.state';
import { Module } from 'vuex';

export const carMock: Module<CarState, RootStoreType> = {
	state: {
		cars: [],
		lightCars: [],
		activeCar: null,
		loading: false,
		stats: {
			total:0
		}
	},
	getters: {
		cars: jest.fn(),
		lightCars: jest.fn(),
		activeCar: jest.fn(),
		loading: jest.fn(),
		stats: jest.fn().mockReturnValue({ total: 0})
	}
}