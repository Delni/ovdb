import { RootStoreType } from 'src/store';
import { ManufacturerState } from 'src/store/manufacturer/manufacturer.state';
import { Module } from 'vuex';

export const manufacturerMock: Module<ManufacturerState, RootStoreType> = {
	state: {
		manufacturers: [],
		loading: false
	},
	getters: {
		// loading: jest.fn(),
		manufacturers: jest.fn()
	}
}
