import { firstBy, isNotId, Sorting } from '../../../../src/middlewares'
import { Entity, Manufacturer } from '../../../../src/store/model'

describe('isNotId', () => {
    const entity: Entity = {id: 'test'}
    const isNotIdTest = isNotId('test')
    it('should return a function', () => {
        expect(isNotIdTest).toBeInstanceOf(Function)
    })

    it('should return false with same id', () => {
        expect(isNotIdTest(entity)).toBeFalsy()
    })

    it('should return true with different id', () => {
        expect(isNotId('not-test')(entity)).toBeTruthy()
    })
})

describe('firstBy', () => {
    const manufacturerA: Manufacturer = {id: '1', name: 'a'}
    const manufacturerB: Manufacturer = {id: '2', name: 'b'}

    describe('When comparing "a" and "b"', () => {
        it('should return -1 if a is first', () => {
            expect(firstBy<Manufacturer>('name')(manufacturerA, manufacturerB)).toBe(-1)
        })

        it('should return 1 if b is first', () => {
            expect(firstBy<Manufacturer>('name')(manufacturerB, manufacturerA)).toBe(1)
        })

        it('should return 0 in case of equality', () => {
            expect(firstBy<Manufacturer>('name')(manufacturerA, manufacturerA)).toBe(0)
        })
    })

    it('should revert order if DESC is provided', () => {
        expect(firstBy<Manufacturer>('id')(manufacturerA, manufacturerB)).toBe(-1)
        expect(firstBy<Manufacturer>('id', Sorting.DESC)(manufacturerA, manufacturerB)).toBe(1)
    })
})