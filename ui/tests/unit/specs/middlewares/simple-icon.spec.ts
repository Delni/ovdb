import { getIcon } from '../../../../src/middlewares'

describe('Simple Icons Middleware', () => {
    it('should return undefined with no slug provided', () => {
        expect(getIcon()).toBeUndefined()
    })

    it('should return undefined with no-corresponding slug', () => {
        expect(getIcon('test')).toBeUndefined()
    })

    it('should return a Simple Icon with proper slug', () => {
        expect(getIcon('ford')).toBeDefined()
    })
})