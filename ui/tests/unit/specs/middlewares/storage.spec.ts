import { clearStore, getStore, setStore } from '../../../../src/middlewares'

describe('Storage middleware', () => {
    describe('setStorage', () => {
        it('Should return undefined with empty name', () => {
            expect(setStore('', null)).toBeUndefined()
        })
        it.todo('Should set max Age when given')
    })


    describe('getStorage', () => {
        const getItemMock =  jest.fn().mockImplementation((name : string) => {
            if(name === 'expired_expire') {
                return 0 // 0 millisecond will always be less than now
            }
            if(name.includes('_expire')) {
                return new Date().getMilliseconds() / 1000 + 100 // 100 seconds from now
            }
            return name
        })

        window.localStorage.getItem = getItemMock

        beforeEach(() => {
            getItemMock.mockReset()
        })

        it('Should return undefined with empty name', () => {
            expect(getStore('')).toBeUndefined()
            expect(getItemMock).not.toHaveBeenCalled()
        })

        it.skip('Should return undefined if expired', () => {
            console.log(window.localStorage.getItem('expired_expire'))
            expect(getStore('expired')).toBeUndefined()
            expect(getItemMock).toHaveBeenNthCalledWith(2, 'expired', 'expired_expire')
        })

        it.todo('Should  return undefined when content is empty')
        it.todo( 'Should return unexpried content')
    })

    describe('clearStorage', () => {
        it('Should return undefined with empty name', () => {
            expect(clearStore('')).toBeUndefined()
        })
        it.todo('Should remove item and its expire date')
    })

    describe('clearAll', () => {
        it.todo('Should clear all storage')
    })
})