import { i18nizer } from '../../../../src/middlewares'

describe('Language global config', function () {
    it('should fallback to fr', () => {
        expect(i18nizer.global.fallbackLocale.value).toBe('fr')
    })
})