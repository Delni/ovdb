import {normalizedNumber} from "../../../../src/middlewares";

describe('normalizeNumber', () => {
    it('should not change number below 1K', () => {
        expect(normalizedNumber(10)).toBe(10)
    })

    it('should append K suffix for value beyond 1K', () => {
        expect(normalizedNumber(2500)).toBe('2.5K')
    })

    it('should append M suffix for value beyond 1M', () => {
        expect(normalizedNumber(2500000)).toBe('2.5M')
    })
})