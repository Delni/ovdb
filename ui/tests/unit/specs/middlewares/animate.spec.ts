import {easeOutCubic, easeOutQuad} from "src/middlewares";

describe('easeOutQuad', () => {
    it('should return 0 with 0', () => {
        expect(easeOutQuad(0)).toBe(0)
    })

    it('should return 1 with 1', () => {
        expect(easeOutQuad(1)).toBe(1)
    })

    it('should return 0.75 half way', () => {
        expect(easeOutQuad(0.5)).toBe(0.75)
    })
})

describe('easeOutCubic', () => {
    it('should return 0 with 0', () => {
        expect(easeOutCubic(0)).toBe(0)
    })

    it('should return 1 with 1', () => {
        expect(easeOutCubic(1)).toBe(1)
    })

    it('should return 0.875 half way', () => {
        expect(easeOutCubic(0.5)).toBe(0.875)
    })
})