import {mount} from '@vue/test-utils'
import Logo from 'src/components/svg/Logo.vue'
import LogoFull from 'src/components/svg/LogoFull.vue'
import SvgBase from 'src/components/svg/SvgBase.vue'
import { createStoreMocks } from '../../../utils'

describe('Logo', () => {
    const wrapper = mount(Logo, {
        shallow: true,
    })

    it('should rely on SvgBase', () => {
        expect(	wrapper.findComponent(SvgBase)).toBeTruthy
	})
	
})

describe('LogoFull', () => {
    const wrapper = mount(LogoFull, {
        shallow: true,
        global: {
			plugins: [createStoreMocks()]
		}
    })

    it('should rely on SvgBase', () => {
        expect(	wrapper.findComponent(SvgBase)).toBeTruthy
	})
	
})