import {mount} from '@vue/test-utils'
import loader from 'src/components/loader.vue'

describe('Loader', () => {
    const wrapper = mount(loader, {
        shallow: true
    })

    it('should have size 1 by default', () => {
        expect(	wrapper.html()).toContain('scale(1)')
	})
	
	it('should scale accordingly with its props', async () => {
		await wrapper.setProps({size: 2})
		// @ts-ignore
		expect(wrapper.vm.size).toBe(2)
	})
})