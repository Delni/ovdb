import {isManufacturer} from "../../../../src/store/model";

test('isManufacturer', () => {
    expect(isManufacturer({})).toBeFalsy()
    expect(isManufacturer({name: 'test'})).toBeTruthy()
})