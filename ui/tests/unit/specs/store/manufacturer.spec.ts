import { ManufacturerMutationsTree } from 'src/store/manufacturer/manufacturer.mutations'
import { ManufacturerState } from 'src/store/manufacturer/manufacturer.state'
import { Manufacturer } from 'src/store/model'
import { state as realState } from 'src/store/manufacturer/manufacturer.state'

const payload: Manufacturer[] = [
	{id: '1', name: 'Toyota'}, 
	{id: '2', name: 'Ford'}
]

describe('Manufacturer store', () => {

	// it.skip('should be namespaced', () => {
	// 	expect(manufacturerStore.namespaced).toBeTruthy()
	// 	expect(manufacturerStore.state).toEqual(realState)
	// })

	const state: ManufacturerState = {
		loading: false, 
		manufacturers: []
	}

	describe('mutations', () => {
		test('LOAD should set load to true', () => {
			ManufacturerMutationsTree.LOAD(state)
			expect(state.loading).toBeTruthy()
		})

		test('STOP_LOAD should set load to true', () => {
			ManufacturerMutationsTree.LOAD(state)
			ManufacturerMutationsTree.STOP_LOAD(state)
			expect(state.loading).toBeFalsy()
		})

		test('SET_MANUFACTURERS should populate manufacturers', () => {
			ManufacturerMutationsTree.SET_MANUFACTURERS(state, payload)
			expect(state.manufacturers).toHaveLength(2)
		})

		test('ADD_MANUFACTURER should add to the state', () => {
			ManufacturerMutationsTree.ADD_MANUFACTURER(state, {name: 'Opel'})
			expect(state.manufacturers).toContainEqual({name: 'Opel'})
		})

		describe('UPDATE_MANUFACTURER', () => {
			it('should do nothing with undefined payload', () => {
				ManufacturerMutationsTree.SET_MANUFACTURERS(state, payload)
				ManufacturerMutationsTree.UPDATE_MANUFACTURER(state, undefined as unknown as Manufacturer)
				expect(state.manufacturers).toHaveLength(2)
			})
			
			it('should update given manufacturer by its id', () => {
				ManufacturerMutationsTree.SET_MANUFACTURERS(state, [payload[0]])
				ManufacturerMutationsTree.UPDATE_MANUFACTURER(state, {
					...payload[0],
					slug: 'test'
				})
				expect(state.manufacturers).toHaveLength(1)
				expect(state.manufacturers[0]).toEqual({id: '1', name: 'Toyota', slug: 'test'})
			})
		});

		test('REMOVE_MANUFACTURER should truncate list', () => {
			ManufacturerMutationsTree.SET_MANUFACTURERS(state, payload)
			expect(state.manufacturers).toHaveLength(2)
			ManufacturerMutationsTree.REMOVE_MANUFACTURER(state, '1')
			expect(state.manufacturers).toHaveLength(1)

		})
	})
})