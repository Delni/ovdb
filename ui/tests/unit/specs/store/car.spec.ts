import {CarState, state} from 'src/store/car/car.state';
import {Car, CarClassification} from 'src/store/model';
import {CarMutations, CarMutationsTree} from 'src/store/car/car.mutations';
import {actions, CarActionContext} from 'src/store/car/car.actions';
import {carStore} from 'src/store/car/car.store';

const payload = [{
	id: 'test-id',
	classification: CarClassification.SUBCOMPACT,
	model: 'Yaris',
	manufacturer: { name: 'Toyota' }
}]
describe('Car store', () => {
	it('should be namespaced', () => {
		expect(carStore.namespaced).toBeTruthy()
	})
	
	it('should have the default state', () => {
		expect(state.loading).toBeFalsy()
		expect(state.activeCar).toBeNull()
		expect(state.cars).toHaveLength(0)
	})

	describe('mutations', () => {
		const state: CarState = {
			activeCar: null,
			cars: [],
			lightCars: [],
			loading: false,
			stats: {
				total: 0
			}
		}

		test('LOAD should set load to true', () => {
			CarMutationsTree.LOAD(state)
			expect(state.loading).toBeTruthy()
		})

		test('STOP_LOAD should set load to true', () => {
			CarMutationsTree.LOAD(state)
			CarMutationsTree.STOP_LOAD(state)
			expect(state.loading).toBeFalsy()
		})

		test('SET_COLLECTIONS should populate array', () => {
			CarMutationsTree.SET_CARS(state, payload)
			expect(state.cars).toHaveLength(1)
			expect(state.cars).toContain(payload[0])
		});

		test('SET_ACTIVE_CAR should populate activeCar', () => {
			expect(state.activeCar).toBeNull()
			CarMutationsTree.SET_ACTIVE_CAR(state, payload[0])
			expect(state.activeCar).toEqual(payload[0])
		})

		test('RESET_ACTIVE_CAR shoud set activeCar to null', () => {
			CarMutationsTree.SET_ACTIVE_CAR(state, payload[0])
			expect(state.activeCar).toEqual(payload[0])
			CarMutationsTree.RESET_ACTIVE_CAR(state)
			expect(state.activeCar).toBeNull()
		})

		test('ADD_COLLECTION should expand state', () => {
			const length = state.cars.length
			CarMutationsTree.ADD_CAR(state, {} as Car)
			expect(state.cars).toHaveLength(length + 1)
		})

		describe('UPDATE_CAR', () => {
			
			it('should do nothing with no payload', () => {
				CarMutationsTree.SET_CARS(state, payload)
				CarMutationsTree.UPDATE_CAR(state, undefined as unknown as Car)
				expect(state.cars).toHaveLength(1)
				expect(state.cars[0].classification).toBe(CarClassification.SUBCOMPACT)
			}) 
			
			it('should update 1 car', () => {
				CarMutationsTree.SET_CARS(state, payload)
				CarMutationsTree.UPDATE_CAR(state, {
					...payload[0], 
					classification: CarClassification.COMPACT
				})
				expect(state.cars).toHaveLength(1)
				expect(state.cars[0].classification).toBe(CarClassification.COMPACT)
			})
		})

		

		test('REMOVE_CAR should empty with the given id', () => {
			CarMutationsTree.SET_CARS(state, payload)
			expect(state.cars).toHaveLength(1)
			CarMutationsTree.REMOVE_CAR(state, 'test-id')
			expect(state.cars).toHaveLength(0)
		})
	})

	describe('actions', () => {
		const state: CarState = {
			activeCar: null,
			cars: [],
			lightCars: [],
			loading: false,
			stats: {
				total: 0
			}
		}

		const commit = jest.fn()

		const mockActionContext: CarActionContext = {
			dispatch: jest.fn(),
			commit,
			getters: {},
			state,
			rootState: {},
			rootGetters: {}
		}

		beforeEach(() => {
			commit.mockClear()
		})

		test.skip('FETCH_CARS should load, set and stop load', async () => {
			await actions.fetchCars(mockActionContext)
			expect(commit).toHaveBeenCalledTimes(3)
			expect(commit).toHaveBeenLastCalledWith(
				CarMutations.STOP_LOAD
			)
		})

		test.todo('SAVE_CAR')
		test.todo('DELETE_CAR')
		describe('LOAD_BY_ID', () => {
			it('should set active car to new object with id "create"', async () => {
				await actions.loadCarById(mockActionContext, 'create')
				expect(commit).toHaveBeenCalledWith(CarMutations.SET_ACTIVE_CAR, { model: ''})
			})

			it('should set active car with existing one if found', async () => {
				mockActionContext.state.cars.push(payload[0])
				await actions.loadCarById(mockActionContext, 'test-id')
				expect(commit).toHaveBeenCalledWith(CarMutations.SET_ACTIVE_CAR, payload[0])
			})

			it.todo('should fetch from api otherwise')
		})
	});
})