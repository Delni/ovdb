import {shallowMount} from '@vue/test-utils'
import App from 'src/App.vue'
import OvHeader from 'src/views/_structure/header.vue'

describe('App', () => {
    const wrapper = shallowMount(App, {})

    it('should have a header', () => {
        expect(wrapper.findComponent(OvHeader)).toBeTruthy()
    })
})