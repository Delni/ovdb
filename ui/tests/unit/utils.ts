import { createStore } from 'vuex'
import { carMock } from './__mocks__/car.mock'
import { manufacturerMock } from './__mocks__/manufacturer.mock'


export function createStoreMocks(
	custom = { getters: {}, mutations: {}, actions: {}, state: {} }
) {
	const mockMutations = { ...custom.mutations}
	const mockActions = {...custom.actions }
	const mockState = {...custom.state }

	return createStore({
			getters: {
				rootLoading: jest.fn()
			},
			mutations: mockMutations,
			actions: mockActions,
			state: mockState,
			modules: {
				'@car': carMock,
				'@manufacturer': manufacturerMock
			}
		})
}
