declare module "*.vue" {
    import {defineComponent} from 'vue'
    const Component: DefineComponent;
    export default Component;
}