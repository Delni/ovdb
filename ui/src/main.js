import { createApp } from 'vue'
import Venus from './assets/venus'
import App from './App.vue'
import {router} from './routes'
import './assets/style/main.sass'
import '@fortawesome/fontawesome-free/js/all'
import './assets/venus/style/themes/transitions.sass'
import { i18nizer } from './middlewares'
import {store} from "./store";

createApp(App)
	.use(Venus)
	.use(i18nizer)
	.use(router)
	.use(store)
	.mount('#app')
