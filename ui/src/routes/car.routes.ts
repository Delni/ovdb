import {Component} from "vue"
import {RouteRecordRaw} from "vue-router"
import Cars from '../views/car/cars.vue'
import CarDetail from '../views/car/car-detail.vue'

export default [
    {
        name: 'cars',
        path: 'cars',
        component: Cars as unknown as Component
    },
    {
        name: 'car-detail',
        path: 'cars/:id',
        component: CarDetail as unknown as Component
    }
] as RouteRecordRaw[]