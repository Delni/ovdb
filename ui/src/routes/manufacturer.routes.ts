import {Component} from "vue";
import {RouteRecordRaw} from "vue-router";
import Manufacturers from '../views/manufacturer/manufacturers.vue'

export default {
    name: 'manufacturers',
    path: 'manufacturers',
    component: Manufacturers as unknown as Component
} as RouteRecordRaw