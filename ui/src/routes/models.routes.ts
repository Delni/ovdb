import {RouteRecordRaw} from "vue-router";
import Models from '../views/models/models.vue'
import {Component} from "vue";

export const modelsRoutes: RouteRecordRaw[] = [
    {
        name: 'models',
        path: '/models',
        component: Models as unknown as Component
    }
]