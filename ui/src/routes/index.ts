import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import Dashboard from '../views/_structure/dashboard.vue'
import Home from '../views/home/home.vue'
import Search from '../views/search/search.vue'
import manufacturersRoutes from "./manufacturer.routes";
import carsRoutes from "./car.routes";
import { Component } from 'vue';
import {modelsRoutes} from "./models.routes";

export const routes: RouteRecordRaw[] = [
	{
		path: '/',
		name: 'dashboard',
		component: Dashboard as unknown as Component,
		redirect: 'home',
		children: [
			{
				// will match everything
				path: '*',
				redirect: { name : 'home' }
			},
			{
				path: 'home',
				name: 'home',
				component: Home as unknown as Component
			},
			{
				path: 'search',
				name: 'search',
				component: Search as unknown as Component
			},
			...carsRoutes,
			...modelsRoutes,
			manufacturersRoutes
		]
	}
]

export const router = createRouter({
	history: createWebHistory(),
	routes: routes
})