import {Manufacturer} from "../model";

export type ManufacturerState = {
    manufacturers: Manufacturer[],
    loading: boolean
}

export const state: ManufacturerState = {
    manufacturers: [],
    loading: false
}