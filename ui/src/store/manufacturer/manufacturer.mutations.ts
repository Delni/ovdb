import { ManufacturerState } from './manufacturer.state'
import { Manufacturer } from '../model'
import { MutationTree } from 'vuex'
import { firstBy, isNotId } from '../../middlewares'

export enum ManufacturerMutations {
    LOAD = 'LOAD',
    STOP_LOAD = 'STOP_LOAD',
    SET_MANUFACTURERS = 'SET_MANUFACTURERS',
    ADD_MANUFACTURER = 'ADD_MANUFACTURER',
    UPDATE_MANUFACTURER = 'UPDATE_MANUFACTURER',
    REMOVE_MANUFACTURER = 'REMOVE_MANUFACTURER'
}

export type ManufacturerMutationType<S = ManufacturerState> = {
    [ManufacturerMutations.LOAD](state: S): void
    [ManufacturerMutations.STOP_LOAD](state: S): void
    [ManufacturerMutations.SET_MANUFACTURERS](state: S, payload: Manufacturer[]): void
    [ManufacturerMutations.ADD_MANUFACTURER](state: S, payload: Manufacturer): void
    [ManufacturerMutations.UPDATE_MANUFACTURER](state: S, payload: Manufacturer): void
    [ManufacturerMutations.REMOVE_MANUFACTURER](state: S, payload: string): void
}

export const ManufacturerMutationsTree: MutationTree<ManufacturerState> & ManufacturerMutationType = {
    [ManufacturerMutations.LOAD](state: ManufacturerState): void {
        state.loading = true
    },

    [ManufacturerMutations.STOP_LOAD](state: ManufacturerState): void {
        state.loading = false
    },

    [ManufacturerMutations.SET_MANUFACTURERS](state: ManufacturerState, payload: Manufacturer[]): void {
        state.manufacturers = [ ...payload ].sort(firstBy<Manufacturer>('name'))
    },

    [ManufacturerMutations.ADD_MANUFACTURER](state: ManufacturerState, payload?: Manufacturer): void {
        if (payload != undefined) {
            state.manufacturers.push(payload)
            state.manufacturers.sort(firstBy<Manufacturer>('name'))
        }
    },

    [ManufacturerMutations.UPDATE_MANUFACTURER](state: ManufacturerState, payload?: Manufacturer): void {
        if (payload != undefined) {
            state.manufacturers = [ 
                ...state.manufacturers.filter(isNotId(payload.id)), 
                payload 
            ]
            .sort(firstBy<Manufacturer>('name'))
        }
    },

    [ManufacturerMutations.REMOVE_MANUFACTURER](state: ManufacturerState, payload: string): void {
        state.manufacturers = [ ...state.manufacturers.filter(isNotId(payload)) ]
    },
}