import { ManufacturerState } from './manufacturer.state'
import { ActionContext, ActionTree } from 'vuex'
import { RootStoreType } from '../index'
import { ManufacturerMutations } from './manufacturer.mutations'
import { manufacturerService } from '../../http/manufacturer.http'
import { Manufacturer } from '../model'

export type ManufacturerActionContext = ActionContext<ManufacturerState, RootStoreType>

export enum ManufacturerActions {
    FETCH_MANUFACTURERS = 'fetchManufacturers',
    SAVE_MANUFACTURER = 'saveManufacturer',
    DELETE_MANUFACTURER = 'deleteManufacturer'
}

export type ManufacturerActionType<V = ManufacturerActionContext> = {
    [ManufacturerActions.FETCH_MANUFACTURERS](store: V): Promise<void>
    [ManufacturerActions.SAVE_MANUFACTURER](store: V, payload: Manufacturer): Promise<void>
    [ManufacturerActions.DELETE_MANUFACTURER](store: V, payload: string): Promise<void>
}

export const actions: ActionTree<ManufacturerState, RootStoreType> & ManufacturerActionType = {
    async [ManufacturerActions.FETCH_MANUFACTURERS]({commit}: ManufacturerActionContext): Promise<void> {
        const {STOP_LOAD, LOAD, SET_MANUFACTURERS} = ManufacturerMutations
        commit(LOAD)
        try {
            const manufacturers = await manufacturerService.getManufacturers()
            commit(SET_MANUFACTURERS, manufacturers)
        } catch(e) {
            console.error(e)
        } finally {
            commit(STOP_LOAD)
        }
    },

    async [ManufacturerActions.SAVE_MANUFACTURER]({commit}: ManufacturerActionContext, payload: Manufacturer): Promise<void> {
        const {ADD_MANUFACTURER, UPDATE_MANUFACTURER} = ManufacturerMutations
        const savedManufacturer = await manufacturerService.saveManufacturer(payload)
        if(payload.id !== undefined) {
            commit(UPDATE_MANUFACTURER, savedManufacturer)
        } else {
            commit(ADD_MANUFACTURER, savedManufacturer)
        }
    },

    async [ManufacturerActions.DELETE_MANUFACTURER]({commit}: ManufacturerActionContext, payload: string): Promise<void> {
        const {REMOVE_MANUFACTURER} = ManufacturerMutations
        await manufacturerService.delete(payload)
        commit(REMOVE_MANUFACTURER, payload)
    },

}