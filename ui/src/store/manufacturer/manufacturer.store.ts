import {Module} from "vuex";
import {ManufacturerState, state} from "./manufacturer.state";
import {RootStoreType} from "../index";
import {ManufacturerMutationsTree} from "./manufacturer.mutations";
import {actions} from "./manufacturer.actions";
import { gettersFactory } from '../../middlewares/index';

export const manufacturerStore: Module<ManufacturerState, RootStoreType> = {
    namespaced: true,
    state,
    mutations: ManufacturerMutationsTree,
    actions,
    getters: gettersFactory(state)
}