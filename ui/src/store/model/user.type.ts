export type User = {
    login: string,
    firstname?: string,
    lastname?: string,
}