import { Entity } from './entity.type'

export type Manufacturer = Entity & {
    name: string,
    slug?: string,
}

export type LightManufacturer = Pick<Manufacturer, 'name' | 'slug'>

export const isManufacturer = (value: unknown): value is Manufacturer => {
    return !!(value as Manufacturer).name
}