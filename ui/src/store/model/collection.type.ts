import {Entity} from "./entity.type";

export type Collection = Entity & {
    name: string
    models?: number
    amount?: number
}