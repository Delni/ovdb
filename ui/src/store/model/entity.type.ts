export type Entity = {
	id?: string,
	description?: string
}