import {Manufacturer} from "./manufacturer.type";

export type Vehicle = {
    image?: string,
    model: string
    manufacturer: Manufacturer
    fabricationStart?: string
    fabricationEnd?: string
}