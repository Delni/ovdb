import {Vehicle} from "./vehicle.type";
import {Entity} from "./entity.type";
import {LightManufacturer} from "./manufacturer.type";

export type Car = Vehicle & Entity & { classification: CarClassification }
export type LightCar = Pick<Car, 'model' | 'id'> & { manufacturer: LightManufacturer }
export type CarStats = {
    total: number
}

export enum CarClassification {
    MICROCAR = 'MICROCAR',
    MINICOMPACT = 'MINICOMPACT',
    SUBCOMPACT = 'SUBCOMPACT',
    COMPACT = 'COMPACT',
    MID_SIZE = 'MID_SIZE',
    FULL_SIZE = 'FULL_SIZE',
    FULL_SIZE_LUXURY = 'FULL_SIZE_LUXURY',
    MPV_MINI = 'MPV_MINI',
    MPV_COMPACT = 'MPV_COMPACT',
    MPV_LARGE = 'MPV_LARGE',
    PREMIUM_COMPACT = 'PREMIUM_COMPACT',
    LUXURY_COMPACT = 'LUXURY_COMPACT',
    MD_SIZE_LUXURY = 'MD_SIZE_LUXURY',
    LUXURY_SALOON = 'LUXURY_SALOON',
    SPORT = 'SPORT',
    SPORT_SEDAN = 'SPORT_SEDAN',
    SUPERCAR = 'SUPERCAR',
    HYPERCAR = 'HYPERCAR',
    OFF_ROAD = 'OFF_ROAD',
    SUV = 'SUV',
    CUV = 'CUV',
    // American Segment
    MUSCLE = 'MUSCLE',
    PONY = 'PONY',
    PERSONAL_LUXURY = 'PERSONAL_LUXURY',
    SPORT_COMPACT = 'SPORT_COMPACT',
    // European Segment,
    GRAND_TOURER = 'GRAND_TOURER',
    HOT_HATCH = 'HOT_HATCH',
    CONCEPT_CAR = 'CONCEPT_CAR',
    UNKNOWN = 'UNKNOWN'
}