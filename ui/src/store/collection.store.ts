import { Store } from './store';
import { Collection } from './model';
import { isNotId } from '../middlewares/index';
import {collectionService} from "../http/collection.http";


export type CollectionState = {
    collections: Collection[],
    loading: boolean
}

class CollectionStore extends Store<CollectionState> {
    protected data(): CollectionState {
        return {
            loading: false,
            collections: []
        }
    }

    async fetchCollections(): Promise<Collection[]> {
        this.state.loading = true
        const collections: Collection[] = await collectionService.getAll()
        this.state.collections = collections 
        this.state.loading = false
        return collections
    }

    async saveCollection(collection: Collection): Promise<void> {
        this.state.loading = true
        const savedCollection = await collectionService.save(collection)
        this.state.loading = false

        if(savedCollection === undefined) return

        if(!!collection.id) {
            this.update(savedCollection)
        } else {
            this.add(savedCollection)
        }
    }

    async deleteCollection({id}: Collection): Promise<void> {
        if(id === undefined) return

        await collectionService.delete(id)
        this.remove(id)
    }

    private add(collection: Collection): void {
        this.state.collections.push(collection)
    }

    private update(collection: Collection): void {
        this.state.collections = [
            ...this.state.collections.filter(isNotId(collection.id)), 
            collection
        ]
    }

    private remove(id: string): void {
        this.state.collections = this.state.collections.filter(isNotId(id))
    }


}

export const collectionStore = new CollectionStore()