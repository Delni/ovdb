import {Module} from "vuex";
import {RootStoreType} from "../index";
import {CarState, state} from "./car.state";
import {CarMutationsTree} from "./car.mutations";
import {actions} from "./car.actions";
import {gettersFactory} from "../../middlewares";

export const carStore: Module<CarState, RootStoreType> = {
    namespaced: true,
    state,
    mutations: CarMutationsTree,
    actions,
    getters: gettersFactory(state)
}