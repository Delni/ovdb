import {CarState} from "./car.state";
import {Car, CarStats, LightCar} from "../model";
import {MutationTree} from "vuex";
import {isNotId} from "../../middlewares";

export enum CarMutations {
    LOAD = 'LOAD',
    STOP_LOAD = 'STOP_LOAD',
    SET_CARS = 'SET_CARS',
    SET_CARS_LIGHT = 'SET_CARS_LIGHT',
    SET_ACTIVE_CAR = 'SET_ACTIVE_CAR',
    RESET_ACTIVE_CAR = 'RESET_ACTIVE_CAR',
    ADD_CAR = 'ADD_CAR',
    UPDATE_CAR = 'UPDATE_CAR',
    REMOVE_CAR = 'REMOVE_CAR',
    SET_STATS = 'SET_STATS'
}

export type CarMutationType<S = CarState> = {
    [CarMutations.LOAD](state: S): void
    [CarMutations.STOP_LOAD](state: S): void
    [CarMutations.SET_CARS](state: S, payload: Car[]): void
    [CarMutations.SET_CARS_LIGHT](state: S, payload: LightCar[]): void
    [CarMutations.SET_ACTIVE_CAR](state: S, payload: Car): void
    [CarMutations.RESET_ACTIVE_CAR](state: S): void
    [CarMutations.ADD_CAR](state: S, payload: Car): void
    [CarMutations.UPDATE_CAR](state: S, payload: Car): void
    [CarMutations.REMOVE_CAR](state: S, payload: string): void
    [CarMutations.SET_STATS](state: S, payload: CarStats): void
}

export const CarMutationsTree: MutationTree<CarState> & CarMutationType = {
    [CarMutations.LOAD](state: CarState): void {
        state.loading = true
    },
    [CarMutations.STOP_LOAD](state: CarState): void {
        state.loading = false
    },
    [CarMutations.ADD_CAR](state: CarState, payload: Car): void {
        const {model, id, manufacturer} = payload
        if(state.stats) {
            state.stats.total++
        }
        state.cars.push(payload)
        state.lightCars.push({
            model,
            id,
            manufacturer
        })
    },
    [CarMutations.REMOVE_CAR](state: CarState, payload: string): void {
        if (state.stats) {
            state.stats.total--

        }
        state.cars = [...state.cars.filter(isNotId(payload))]
        state.lightCars = [...state.lightCars.filter(isNotId(payload))]
    },
    [CarMutations.SET_ACTIVE_CAR](state: CarState, payload: Car): void {
        state.activeCar = payload
    },
    [CarMutations.RESET_ACTIVE_CAR](state: CarState): void {
        state.activeCar = null
    },
    [CarMutations.SET_CARS](state: CarState, payload: Car[]): void {
        state.cars = [...payload]
    },
    [CarMutations.SET_CARS_LIGHT](state: CarState, payload: LightCar[]): void {
        state.lightCars = [...payload]
    },
    [CarMutations.UPDATE_CAR](state: CarState, payload?: Car): void {
        if (payload != undefined) {
            state.cars = [
                ...state.cars.filter(isNotId(payload.id)),
                payload
            ]
        }
    },
    [CarMutations.SET_STATS](state: CarState, payload: CarStats): void {
        state.stats = payload
    }
}