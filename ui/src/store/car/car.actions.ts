import {ActionContext, ActionTree} from "vuex";
import {CarState} from "./car.state";
import {RootStoreType} from "../index";
import {Car, CarStats} from "../model";
import {CarMutations} from "./car.mutations";
import { carService } from '../../http/car.http';

export type CarActionContext = ActionContext<CarState, RootStoreType>

export enum CarActions {
    FETCH_CARS = 'fetchCars',
    SAVE_CAR = 'saveCar',
    DELETE_CAR = 'deleteCar',
    LOAD_BY_ID = 'loadCarById',
    LOAD_STATS = 'loadStats'
}

export type CarActionType<C = CarActionContext> = {
    [CarActions.FETCH_CARS](context: C): Promise<void>
    [CarActions.SAVE_CAR](context: C, payload: Car): Promise<Car | undefined>
    [CarActions.DELETE_CAR](context: C, payload: string): Promise<void>
    [CarActions.LOAD_BY_ID](context: C, payload: string): Promise<void>
    [CarActions.LOAD_STATS](context: C): Promise<CarStats>
}

export const actions: ActionTree<CarState, RootStoreType> & CarActionType = {
    async [CarActions.FETCH_CARS]({commit}: CarActionContext) {
        const {STOP_LOAD,LOAD, SET_CARS_LIGHT} = CarMutations
        commit(LOAD)
        commit(SET_CARS_LIGHT, await carService.getAll())
        commit(STOP_LOAD)
    },

    async [CarActions.SAVE_CAR]({state, commit, dispatch}: CarActionContext, payload: Car) {
        const {LOAD, STOP_LOAD, ADD_CAR, UPDATE_CAR} = CarMutations;
        commit(LOAD)
        const car = await carService.save(payload)
        if(!payload.id) {
            commit(ADD_CAR, car)
        } else {
            commit(UPDATE_CAR, car)
        }
        commit(STOP_LOAD)
        dispatch(CarActions.LOAD_STATS)
        return car
    },

    async [CarActions.DELETE_CAR]() {},

    async [CarActions.LOAD_BY_ID]({state, commit}: CarActionContext, payload: string) {
        const {SET_ACTIVE_CAR, LOAD, STOP_LOAD} = CarMutations;
        commit(LOAD)

        if(payload === 'create') {
            const emptyCar: Car = {
                model: '',
            } as Car
            commit(SET_ACTIVE_CAR, emptyCar)
        } else {
            const fromState = state.cars.find(({id}: Car) => id === payload)

            if(fromState !== undefined) {
                commit(SET_ACTIVE_CAR, fromState)
            } else {
                commit(SET_ACTIVE_CAR, await carService.get(payload))
            }
        }

        commit(STOP_LOAD)
    },

    async [CarActions.LOAD_STATS]({commit}: CarActionContext) {
        const stats = await carService.stats()
        commit(CarMutations.SET_STATS, stats)
        return stats
    }
}