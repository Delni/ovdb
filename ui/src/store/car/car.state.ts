import {Car, CarStats, LightCar} from "../model";

export type CarState = {
    cars: Car[],
    lightCars: LightCar[],
    activeCar: Car | null
    loading: boolean,
    stats: CarStats | null
}

export const state: CarState = {
    cars: [],
    lightCars: [],
    activeCar: null,
    loading: false,
    stats: null
}