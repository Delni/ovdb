import {Module, MutationTree} from "vuex";
import {User} from "./model";
import {RootStoreType} from "./index";

export type UserState = {
    me?: User
}
const state: UserState = {
    me: undefined
}

export enum UserMutations {
    SET_USER = "SET_USER"
}

export type UserMutationType<S = UserState> = {
    [UserMutations.SET_USER](state: S, payload: User): void
}

const mutations: MutationTree<UserState> & UserMutationType = {
    [UserMutations.SET_USER](state: UserState, payload: User) {
        state.me = payload
    }
}

export const userStore: Module<UserState, RootStoreType> = {
    namespaced: true,
    state,
    mutations,
    getters: {
        me: state => state.me
    },
}