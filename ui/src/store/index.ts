import {createStore} from "vuex";
import {userStore} from "./user.store";
import {manufacturerStore} from "./manufacturer/manufacturer.store";
import {carStore} from "./car/car.store";
import { collectionStore } from './collection.store';

export type RootStoreType = {}
export const store = createStore({
    getters: {
        rootLoading: (state, getters) => {
            return getters['@car/loading']
                || getters['@manufacturer/loading']
                || getters['@collection/loading']
                || collectionStore.getState().loading
                || false
        }
    },
    modules: {
        '@user': userStore,
        '@car': carStore,
        '@manufacturer': manufacturerStore
    }
})