import {client, getData} from '.';
import {Car, CarStats, LightCar} from '../store/model';
import gql from 'graphql-tag'

export const carService = {

    getAll(): Promise<LightCar[]> {
        return client.query<{ cars: LightCar[] }>({
            query: gql`query cars {
                cars {
                    id,
                    model,
                    manufacturer {
                        name,
                        slug
                    }
                }
            }`
        }).then(getData).then(result => result.cars)
    },

    get(id: string): Promise<Car | undefined> {
        return client.query<{ car: Car }>({
            query: gql`query car($id: String!) {
                car(id: $id) {
                    id,
                    model,
                    description,
                    image,
                    manufacturer {
                        id,
                        name,
                        slug
                    },
                    classification,
                    fabricationStart,
                    fabricationEnd
                }
            }`,
            variables: {
                id
            }
        }).then(getData).then(result => result ? Object.assign({}, result.car) : undefined)
    },

    save(car: Car): Promise<Car | undefined> {
        return client.mutate<{ saveCar: Car }>({
            mutation: gql`mutation saveCar($car: CarInput!) {
                saveCar(car: $car) {
                    id,
                    model,
                    manufacturer {
                        id,
                        name,
                        slug
                    },
                    classification,
                    fabricationStart,
                    fabricationEnd
                }
            }`,
            variables: {
                car
            }
        })
        .then(getData)
        .then(result => result ? Object.assign({}, result.saveCar) : undefined)
    },

    stats(): Promise<CarStats> {
        return client.query<{carStats: CarStats}>({
            query: gql`query carStats {
                carStats {
                    total
                }
            }`
        }).then(getData).then(result => Object.assign({}, result.carStats))
    }
}