import gql from "graphql-tag";
import {Collection} from "../store/model";
import {client, getData, noop} from "./index";

export const collectionService = {
    getAll(): Promise<Collection[]> {
        return client.query<{ collections: Collection[] }>({
            query: gql`query {
                collections {
                    id,
                    name,
                    models,
                    amount
                }
            }
            `
        }).then(getData).then(result => [...result.collections])
    },
    findById(id: string) {},
    save(collection: Collection): Promise<Collection | undefined> {
        return client.mutate<{ saveCollection: Collection }>({
            mutation: gql`mutation saveCollection($collection: CollectionInput!) {
                saveCollection(collection: $collection) {
                    id,
                    name
                }
            }`,
            variables: {
                collection
            }
        })
        .then(getData)
        .then((result) => result?.saveCollection)
    },
    delete(id: string): Promise<void> {
        return client.mutate({
            mutation: gql`mutation deleteCollection($id: String!) {
                deleteCollection(id: $id)
            }`,
            variables: {
                id
            }
        }).then(noop)
    }
}