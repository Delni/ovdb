import { ApolloClient, ApolloQueryResult, InMemoryCache } from '@apollo/client'
import { FetchResult } from 'apollo-link'

let uri;
if(process.env.NODE_ENV !== 'production') {
    uri = 'http://localhost:4242'
}

export const client = new ApolloClient({
    uri: (uri || '.') + '/graphql',
    cache: new InMemoryCache({
        addTypename: false,
    })
})

export const getData = <T>(value: ApolloQueryResult<T> | FetchResult<T, any, any>): T => value.data as T

export const noop = () => {}