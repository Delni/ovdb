import { Manufacturer } from '../store/model'
import { client, getData, noop } from './index'
import gql from 'graphql-tag'

export const manufacturerService = {
    getManufacturers(): Promise<Manufacturer[]> {
        return client.query<{ manufacturers: Manufacturer[] }>({
            query: gql`query {
                manufacturers {
                    id,
                    name,
                    slug
                }
            }`
        })
        .then(getData)
        .then(({manufacturers}) => manufacturers)
    },

    saveManufacturer(manufacturer: Manufacturer): Promise<Manufacturer | undefined> {
        return client.mutate<{ saveManufacturer: Manufacturer}>({
            mutation: gql`mutation saveManufacturer($manufacturer: ManufacturerInput!) {
                saveManufacturer(manufacturer: $manufacturer) {
                  id,
                  name,
                  slug
                }
              }`,
            variables: {
                manufacturer
            }
        })
        .then(getData)
        .then((result) =>  result?.saveManufacturer)
    },

    delete(id: string): Promise<void> {
        return client.mutate({
            mutation: gql`mutation deleteManufacturer($id: String!) {
                deleteManufacturer(id: $id)
            }`,
            variables: {
                id
            }
        }).then(noop)
    }
}