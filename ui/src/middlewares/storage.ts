export const setStore = (name: string, content: unknown, maxAge: number | null = null) => {
	if (!window || !name) {
		return
	}

	if (typeof content !== 'string') {
		content = JSON.stringify(content)
	}

	const storage = window.localStorage

	storage.setItem(name, content as string)

	if (maxAge) {
		const timeout = new Date().getMilliseconds() / 1000
		storage.setItem(`${name}_expire`, `${timeout + maxAge}`)
	}
}

export const getStore = <T> (name: string): T | undefined => {
	if (!window || !name) {
		return
	}

	const content = window.localStorage.getItem(name) || ''
	const _expire = parseInt(window.localStorage.getItem(`${name}_expire`) || '')

	if (_expire) {
		const now = new Date().getMilliseconds() / 1000
		if (now > _expire) {
			return
		}
	}

	if(!content) {
		return undefined
	}

	try {
		return JSON.parse(content) as T
	} catch (e) {
		console.error(e)
		return content as unknown as T
	}
}

export const clearStore = (name: string) => {
	if (!window || !name) {
		return
	}

	window.localStorage.removeItem(name)
	window.localStorage.removeItem(`${name}_expire`)
}

export const clearAll = () => {
	if (!window) {
		return
	}

	window.localStorage.clear()
}
