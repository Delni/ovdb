import { Entity } from 'src/store/model'

export * from './storage'
export * from './i18nizer'
export * from './constants'
export * from './simple-icon.middleware'
export * from './animate'
export * from './filters'


export function isNotId<T extends Entity>(id?: String): (value: T) => boolean {
	return (value: T) => value.id !== id
}

export enum Sorting {
	ASC = 1,
	DESC = -1
}

export function firstBy<T>(key: keyof T, sorting: Sorting | 1 | -1 = Sorting.ASC): (a: T, b: T) => number {
	return (a: T, b: T) => {
		if(a[key] < b[key]) {
			return -1 * sorting
		}
		if(a[key] > b[key]) {
			return 1 * sorting
		}
		return 0
	}
}

export function gettersFactory<S>(baseState: S): Record<keyof S, (state: S) => unknown> {
	return Object.keys(baseState).reduce((records, key) => {
		// @ts-ignore
		records[key] = (state: S) => state[key]
		return records
	}, {} as Record<keyof S, (state: S) => any>)
}