export function normalizedNumber(n: number): number | string {
    if (n >= 1000000) {
        return (n / 1000000).toFixed(1) + 'M'
    } else if (n >= 1000) {
        return (n / 1000).toFixed(1) + 'K'
    }
    return n
}

export function toHtmlBr(text?: string): string | undefined {
    return text ? text.replace(/\n/,'<br/>') : text
}