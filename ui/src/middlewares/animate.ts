import {Ref} from "vue";

export const easeOutQuad = (t: number): number =>  t * ( 2 - t )
export const easeOutCubic = (t: number): number => 1 - Math.pow(1 - t, 3)

export const animate = (to: number | undefined, target: Ref<number>, curve: (t: number) => number = easeOutCubic)  => {
    let frame = 0;
    const duration = (to ?? 0) > 1000 ? 5000 : 1000;
    const fps = 25
    const frameDuration = 1000 / fps;
    const totalFrames = Math.round( duration / frameDuration );
    const counter = setInterval( () => {
        frame++;
        const progress = curve( frame / totalFrames );
        target.value = Math.round((to ?? 0) * progress)

        if ( frame === totalFrames ) {
            clearInterval( counter );
        }
    }, frameDuration );
}