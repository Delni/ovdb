import icons, { SimpleIcon } from 'simple-icons'

export const getIcon = (slug?: string): SimpleIcon | undefined => icons.get(slug || '')