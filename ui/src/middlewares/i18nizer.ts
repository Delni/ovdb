import { createI18n } from 'vue-i18n'

import fr from '../../public/lang/fr.json'
import { getStore } from './storage'
import { LANG } from './constants'

export const i18nizer = createI18n({
	locale: getStore<string>(LANG) || navigator.language.split('-')[0],
	fallbackLocale: 'fr',
	messages: {
		fr,
	}
})