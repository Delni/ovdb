import VnHeader from './components/header.vue'
import VnModal from './components/modal.vue'
import VnSelect from './components/select.vue'
import VnEditable from './components/editable.vue'
import './main.sass'

export default {
	install(Vue) {
		Vue.component('VnHeader', VnHeader)
		Vue.component('VnModal', VnModal)
		Vue.component('VnSelect', VnSelect)
		Vue.component('VnEditable', VnEditable)
	}
}