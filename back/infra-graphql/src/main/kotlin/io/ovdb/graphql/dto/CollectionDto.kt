package io.ovdb.graphql.dto

import com.expediagroup.graphql.annotations.GraphQLName
import io.ovdb.domain.collection.Collection
import java.math.BigDecimal

@GraphQLName("Collection")
data class CollectionDto(
    val id: String,
    val name: String,
    val models: Long,
    val amount: BigDecimal,
)

data class CollectionInput(
    val id: String?,
    val name: String
)

fun Collection.toDto() = CollectionDto(
    id = id!!,
    name = name,
    models = models ?: 0L,
    amount = amount ?: BigDecimal.ZERO
)

fun CollectionInput.toDomain() = Collection(
    id = id,
    name = name,
    models = null,
    amount = null,
    description = null
)