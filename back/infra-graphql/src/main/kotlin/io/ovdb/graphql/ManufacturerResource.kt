package io.ovdb.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.manufacturer.ManufacturerService
import io.ovdb.graphql.dto.*
import org.springframework.stereotype.Component

@Component
class ManufacturerQueries(private val service: ManufacturerService): Query {
    fun manufacturers(): List<ManufacturerDto> = service.find().map(Manufacturer::toDto)
    fun manufacturer(id: String?, name: String?) = service.findOne(id, name)?.toDto()
}

@Component
class ManufacturerMutations(private val service: ManufacturerService): Mutation {
    fun saveManufacturer(manufacturer: ManufacturerInput) = service.save(manufacturer.toDomain()).toDto()
    fun deleteManufacturer(id: String) = service.delete(id).let { id }
}