package io.ovdb.graphql.dto

import com.expediagroup.graphql.annotations.GraphQLName
import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarClassification
import io.ovdb.domain.car.Stats

@GraphQLName("Car")
data class CarDto(
    val id: String,
    val classification: CarClassification,
    val manufacturer: ManufacturerDto,
    val model: String,
    val description: String?,
    val image: String?,
    val fabricationStart: String,
    val fabricationEnd: String?
)

@GraphQLName("CarStats")
data class CarStatsDto(
    val total: Long
)

data class CarInput(
    val id: String?,
    val classification: CarClassification,
    val manufacturer: ManufacturerInput,
    val model: String,
    val description: String?,
    val image: String?,
    val fabricationStart: String,
    val fabricationEnd: String?,
)

fun Car.toDto() = CarDto(
    id = id!!,
    model = model,
    description = description,
    classification = classification,
    manufacturer = manufacturer.toDto(),
    fabricationStart = fabricationStart,
    fabricationEnd = fabricationEnd,
    image = image
)

fun Stats.toDto() = CarStatsDto(
    total = total
)

fun CarInput.toDomain() = Car(
    id = id,
    model = model,
    description = description,
    classification = classification,
    manufacturer = manufacturer.toDomain(),
    fabricationStart = fabricationStart,
    fabricationEnd = fabricationEnd,
    image = image
)