package io.ovdb.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import io.ovdb.domain.collection.CollectionService
import io.ovdb.graphql.dto.CollectionDto
import io.ovdb.graphql.dto.CollectionInput
import io.ovdb.graphql.dto.toDomain
import io.ovdb.graphql.dto.toDto
import org.springframework.stereotype.Component


@Component
class CollectionQueries(private val service: CollectionService): Query {
    fun collections(): List<CollectionDto> = service.find().map { it.toDto() }
    fun collection(id: String) : CollectionDto? = service.findById(id)?.toDto()
}

@Component
class CollectionMutations(private val service: CollectionService): Mutation {
    fun saveCollection(collection: CollectionInput) = service.save(collection.toDomain()).toDto()
    fun deleteCollection(id: String) = service.delete(id)
}