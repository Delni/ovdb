package io.ovdb.graphql.dto

import com.expediagroup.graphql.annotations.GraphQLName
import io.ovdb.domain.manufacturer.Manufacturer

@GraphQLName("Manufacturer")
data class ManufacturerDto(
    val id: String?,
    val name: String,
    val slug: String,
    val description: String?
)

data class ManufacturerInput(
    val id: String?,
    val name: String,
    val slug: String?,
    val description: String?
)

fun Manufacturer.toDto() = ManufacturerDto(
    id = id,
    name = name,
    slug = slug ?: defaultSlug,
    description = description
)


fun ManufacturerInput.toDomain() = Manufacturer(
    id= id,
    name= name,
    slug = slug,
    description = description
)