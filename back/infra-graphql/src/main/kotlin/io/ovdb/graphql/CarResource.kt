package io.ovdb.graphql

import com.expediagroup.graphql.spring.operations.Mutation
import com.expediagroup.graphql.spring.operations.Query
import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarService
import io.ovdb.graphql.dto.*
import org.springframework.stereotype.Component

@Component
class CarQueries(private val service: CarService) : Query {
    fun cars(): List<CarDto> = service.find().map(Car::toDto)
    fun car(id: String): CarDto? = service.findById(id)?.toDto()
    fun carStats(): CarStatsDto = service.getStats().toDto()
}

@Component
class CarMutations(private val service: CarService) : Mutation {
    fun saveCar(car: CarInput): CarDto = service.save(car.toDomain()).toDto()
    fun deleteCar(id: String): String = service.delete(id).let { id }
}