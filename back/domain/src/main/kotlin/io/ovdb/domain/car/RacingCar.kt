package io.ovdb.domain.car

import io.ovdb.domain.core.Entity
import io.ovdb.domain.racing.Pilot
import io.ovdb.domain.racing.Race
import io.ovdb.domain.racing.Team

data class RacingCar(
    override val id: String,
    override val description: String? = null,
    val car: Car,
    val races: List<Race> = emptyList(),
    val crew: List<Pilot> = emptyList(),
    val team: Team,
    val arrivalPlace: Int? = null,
    val dropOutReason: String? = null
) : Entity