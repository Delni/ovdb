package io.ovdb.domain.core

interface Entity {
    val id: String?
    val description: String?
}