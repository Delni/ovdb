package io.ovdb.domain.models

import io.ovdb.domain.car.Car
import io.ovdb.domain.core.Entity

data class Model(
    override val id: String?,
    override val description: String?,
    val vehicle: Car,
    val scale: Scale,
    val editor: String,
    val tags: List<String>
) : Entity