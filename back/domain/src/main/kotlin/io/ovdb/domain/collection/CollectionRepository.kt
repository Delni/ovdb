package io.ovdb.domain.collection

interface CollectionRepository {
    fun find(): List<Collection>
    fun findById(id: String): Collection?
    fun save(collection: Collection): Collection
    fun delete(id: String): String
}