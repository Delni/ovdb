package io.ovdb.domain.models

interface ModelRepository {
    fun find(): List<Model>
    fun findById(id: String): Model?
    fun save(model: Model): Model
    fun delete(id: String): String

    fun findInCollection(collectionId: String): List<PossessedModel>
    fun findInCollectionById(collectionId: String, id: String): PossessedModel?
    fun save(collectionId: String, possessedModel: PossessedModel): PossessedModel
    fun removeFromCollection(possessedModelId: String): String

}