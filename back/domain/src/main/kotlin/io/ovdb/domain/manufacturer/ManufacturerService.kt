package io.ovdb.domain.manufacturer

import org.springframework.stereotype.Service

@Service
class ManufacturerService(private val repository: ManufacturerRepository) {
    fun find() = repository.find()

    fun findOne(id: String?, name: String?) = when {
        id != null -> repository.findById(id)
        name != null -> repository.find(name = name).firstOrNull()
        else -> null
    }

    fun save(manufacturer: Manufacturer) = when (manufacturer.id) {
        null -> repository.create(manufacturer)
        else -> repository.update(manufacturer)
    }

    fun delete(id: String) = repository.delete(id)
}