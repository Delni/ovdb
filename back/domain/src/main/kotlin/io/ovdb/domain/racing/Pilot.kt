package io.ovdb.domain.racing

import io.ovdb.domain.core.Entity

data class Pilot(
    override val id: String,
    val firstname: String,
    val lastname: String,
    override val description: String? = null
): Entity