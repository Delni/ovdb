package io.ovdb.domain.car

data class Stats (
    val total: Long
)