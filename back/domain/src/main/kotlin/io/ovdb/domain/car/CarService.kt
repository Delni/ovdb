package io.ovdb.domain.car

import org.springframework.stereotype.Service

@Service
class CarService(private val repository: CarRepository) {
    fun find(name: String? = null,
             classification: CarClassification? = null,
             before: String? = null,
             after: String? = null,
             manufacturer: String? = null
    ) = repository.find(name, classification, before, after, manufacturer)

    fun findById(id: String) = repository.findById(id)
    fun save(car: Car) = car
        .takeIf { it.id == null }
        ?.let { repository.create(it) }
        ?:  repository.update(car)

    fun getStats() = repository.getStats()

    fun delete(id: String) = repository.delete(id)
}