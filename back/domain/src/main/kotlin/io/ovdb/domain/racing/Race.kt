package io.ovdb.domain.racing

import io.ovdb.domain.core.Entity

data class Race(
    override val id: String,
    override val description: String? = null,
    val date: String,
) : Entity