package io.ovdb.domain.collection

import org.springframework.stereotype.Service

@Service
class CollectionService(private val repository: CollectionRepository) {
    fun find() = repository.find()
    fun findById(id: String) = repository.findById(id)

    fun save(collection: Collection) = repository.save(collection)
    fun delete(id: String) = repository.delete(id)
}