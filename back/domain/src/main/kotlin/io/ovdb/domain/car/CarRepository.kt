package io.ovdb.domain.car

interface CarRepository {
    fun find(
        model: String? = null,
        classification: CarClassification? = null,
        before: String? = null,
        after: String? = null,
        manufacturer: String? = null
    ): List<Car>
    fun findById(id: String): Car?
    fun create(car: Car): Car
    fun update(car: Car): Car
    fun getStats(): Stats
    fun delete(id: String)
}