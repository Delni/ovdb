package io.ovdb.domain.models

import io.ovdb.domain.core.Entity
import java.math.BigDecimal

data class PossessedModel(
    val model: Model,
    val boxed: Boolean,
    val location: String?,
    val price: BigDecimal,
    val buyDate: String
): Entity by model