package io.ovdb.domain

import io.ovdb.domain.manufacturer.Manufacturer

interface Vehicle {
    val model: String
    val image: String?
    val manufacturer: Manufacturer
    val fabricationStart: String
    val fabricationEnd: String?
}