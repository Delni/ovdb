package io.ovdb.domain.manufacturer

import io.ovdb.domain.core.Entity
import java.text.Normalizer

data class Manufacturer(
    override val id: String?,
    val name: String,
    val slug: String? = null,
    override val description: String? = null
) : Entity {
    val defaultSlug = Normalizer
        .normalize(name, Normalizer.Form.NFD)
        .replace("\\p{Mn}+".toRegex(), "")
        .toLowerCase()
        .replace(" ", "")
}