package io.ovdb.domain.manufacturer

interface ManufacturerRepository {
    fun find(name: String? = null): List<Manufacturer>
    fun findById(id: String): Manufacturer?
    fun create(manufacturer: Manufacturer): Manufacturer
    fun update(manufacturer: Manufacturer): Manufacturer
    fun delete(id: String)
}