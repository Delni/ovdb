package io.ovdb.domain.models

data class Scale(val ratio: String) {
    companion object {
        val S1_32 = Scale("1/32")
        val S1_43 = Scale("1/43")
        val S1_64 = Scale("1/64")
    }
}