package io.ovdb.domain.car

// see https://en.wikipedia.org/wiki/Car_classification
enum class CarClassification {
    MICROCAR,
    MINICOMPACT,
    SUBCOMPACT,
    COMPACT,
    MID_SIZE,
    FULL_SIZE,
    FULL_SIZE_LUXURY,
    MPV_MINI,
    MPV_COMPACT,
    MPV_LARGE,
    PREMIUM_COMPACT,
    LUXURY_COMPACT,
    MD_SIZE_LUXURY,
    LUXURY_SALOON,
    SPORT,
    SPORT_SEDAN,
    SUPERCAR,
    HYPERCAR,
    OFF_ROAD,
    SUV,
    CUV,
    // American Segment
    MUSCLE,
    PONY,
    PERSONAL_LUXURY,
    SPORT_COMPACT,
    // European Segment,
    GRAND_TOURER,
    HOT_HATCH,
    CONCEPT_CAR,
    UNKNOWN
}