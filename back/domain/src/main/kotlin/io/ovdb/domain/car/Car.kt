package io.ovdb.domain.car

import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.Vehicle
import io.ovdb.domain.core.Entity

data class Car(
    override val id: String?,
    val classification: CarClassification,
    override val manufacturer: Manufacturer,
    override val model: String,
    override val fabricationStart: String,
    override val fabricationEnd: String? = null,
    override val description: String?,
    override val image: String?
): Vehicle, Entity