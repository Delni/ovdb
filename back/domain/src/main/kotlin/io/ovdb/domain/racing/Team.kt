package io.ovdb.domain.racing

import io.ovdb.domain.core.Entity

data class Team(
    override val id: String,
    override val description: String? = null,
    val name: String
) : Entity