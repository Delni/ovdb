package io.ovdb.domain.collection

import io.ovdb.domain.core.Entity
import java.math.BigDecimal

data class Collection(
    override val id: String?,
    val name: String,
    val models: Long? = null,
    val amount: BigDecimal? = null,
    override val description: String?
): Entity
