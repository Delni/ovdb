create table cars (
    car_id text not null,
    car_classification text not null,
    car_manufacturer text not null,
    car_model text not null,
    car_fabrication_start_date timestamp not null,
    car_fabrication_end_date timestamp,
    car_created_date timestamp not null,
    car_edited_date timestamp not null,
    car_deleted_date timestamp,
    constraint car_pk primary key (car_id),
    constraint car_man_fk foreign key (car_manufacturer) references manufacturers(man_id)
)