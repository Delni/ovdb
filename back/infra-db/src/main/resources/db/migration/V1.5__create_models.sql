create table models
(
    mdl_id           text      not null,
    mdl_vehicle_id   text      not null,
    mdl_scale        text      not null,
    mdl_editor       text      not null,
    mdl_tags         text[],
    mdl_description  text      null,
    mdl_created_date timestamp not null,
    mdl_edited_date  timestamp not null,
    mdl_delete_date  timestamp null,
    constraint mdl_pk primary key (mdl_id),
    constraint mdl_car_fk foreign key (mdl_vehicle_id) references cars (car_id)
);

create table models_to_collections
(
    mtc_mdl_id       text      not null,
    mtc_col_id       text      not null,
    mtc_boxed        bool      not null default false,
    mtc_location     text      null,
    mtc_price        text      not null,
    mtc_buy_date     timestamp not null,
    mtc_created_date timestamp not null,
    mtc_edited_date  timestamp not null,
    constraint mtc_pk primary key (mtc_mdl_id, mtc_col_id),
    constraint mtc_col_fk foreign key (mtc_col_id) references collections (col_id),
    constraint mtc_mdl_fk foreign key (mtc_mdl_id) references models (mdl_id)
)