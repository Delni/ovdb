create table collections (
    col_id text not null,
    col_name text not null,
    col_description text null,
    col_created_date timestamp not null,
    col_edited_date timestamp not null,
    col_deleted_date timestamp,
    constraint col_pk primary key (col_id)
)