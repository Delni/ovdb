create table manufacturers (
    man_id text not null,
    man_name text not null,
    man_slug text,
    man_created_date timestamp not null,
    man_edited_date timestamp not null,
    man_deleted_date timestamp,
    constraint man_pk primary key (man_id)
)