create user ovdb with password 'ovdb';
create database ovdb with owner ovdb;
create database ovdb_test with owner ovdb;
\connect ovdb;
create schema authorization ovdb;