package io.ovdb.infra.db

import io.ovdb.domain.collection.Collection
import io.ovdb.domain.collection.CollectionRepository
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service

@Service
class CollectionDao(internal val jdbcTemplate: NamedParameterJdbcTemplate): CollectionRepository {
    override fun find() = jdbcTemplate.queryForList("""
        select * from collections
        where col_deleted_date is null
        """, emptyMap<String, String>()).map(MutableMap<String, Any>::toCollection)

    override fun findById(id: String) = jdbcTemplate.queryForList("""
       select * from collections 
       where col_deleted_date is null and col_id = :id
    """, mapOf("id" to id)).firstOrNull()?.toCollection()

    override fun save(collection: Collection) = collection.run {
        val newId = newId()
        jdbcTemplate.update("""
            insert into collections(
                col_id, 
                col_name, 
                col_description, 
                col_created_date, 
                col_edited_date
            ) values (
                :id,
                :name,
                :description,
                :date,
                :date
            ) on conflict (col_id) do update set
                col_name = :name
        """, mapOf(
            "id" to (id ?: newId),
            "name" to name,
            "description" to description,
            "date" to nowstamp()
        ))
        copy(id = id ?: newId)
    }

    override fun delete(id: String) =  id.also {
        jdbcTemplate.update("""
            update collections set
                col_deleted_date = :date
            where col_id = :id
        """, mapOf("id" to id, "date" to nowstamp()))
    }
}