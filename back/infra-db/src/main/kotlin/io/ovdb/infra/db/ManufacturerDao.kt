package io.ovdb.infra.db

import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.manufacturer.ManufacturerRepository
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service

@Service
class ManufacturerDao(internal val jdbcTemplate: NamedParameterJdbcTemplate): ManufacturerRepository {
    override fun find(name: String?): List<Manufacturer> {
        var request = "select * from manufacturers where  man_deleted_date is NULL "
        name?.let { request += "and man_name = :name " }

        return jdbcTemplate.queryForList(request, mapOf(
            "name" to name
        )).map { it.toManufacturer() }
    }

    override fun findById(id: String) = jdbcTemplate.queryForList(
        """
            select * from manufacturers
            where man_id = :id and man_deleted_date is NULL
        """,
        mapOf("id" to id)
    ).firstOrNull()?.toManufacturer()

    override fun create(manufacturer: Manufacturer) = update(manufacturer.copy(id = newId()))

    override fun update(manufacturer: Manufacturer) = manufacturer.apply {
        jdbcTemplate.update("""
            insert into manufacturers (
                man_id,
                man_name,
                man_slug,
                man_description,
                man_created_date,
                man_edited_date
            ) values (
                :id,
                :name,
                :slug,
                :description,
                :date,
                :date
            ) on conflict (man_id) do update set
                man_name = :name,
                man_slug = :slug,
                man_description = :description,
                man_edited_date = :date
        """,
        mapOf(
            "id" to id,
            "name" to name,
            "slug" to slug,
            "description" to description,
            "date" to nowstamp()
        ))
    }

    override fun delete(id: String) {
        jdbcTemplate.update(
            """
                update manufacturers
                set man_deleted_date = :date
                where man_id = :id
            """,
            mapOf(
                "id" to id,
                "date" to nowstamp(),
            )
        )
    }
}