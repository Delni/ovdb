package io.ovdb.infra.db

import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarClassification
import io.ovdb.domain.collection.Collection
import io.ovdb.domain.models.Model
import io.ovdb.domain.models.PossessedModel
import io.ovdb.domain.models.Scale
import org.postgresql.jdbc.PgArray
import java.math.BigDecimal
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

fun newId() = "uid-" + UUID.randomUUID().toString()
fun nowstamp() = Timestamp(Instant.now().toEpochMilli())

private val formatter = SimpleDateFormat("yyyy-MM-dd")
fun String.toTimestamp(): Timestamp = Timestamp(formatter.parse(this).toInstant().toEpochMilli())
fun Timestamp.toDbString(): String = formatter.format(Date(this.time))

fun Map<String, Any?>.toManufacturer() = Manufacturer(
    id = get("man_id") as String,
    name = get("man_name") as String,
    description = get("man_description") as String?
)

fun Map<String, Any?>.toCar() = Car(
    id = get("car_id") as String,
    classification = CarClassification.valueOf(get("car_classification") as String),
    manufacturer = toManufacturer(),
    model = get("car_model") as String,
    description = get("car_description") as String?,
    image = get("car_image") as String?,
    fabricationStart = (get("car_fabrication_start_date") as Timestamp).toDbString(),
    fabricationEnd = (get("car_fabrication_end_date") as Timestamp?)?.toDbString(),
)

fun Map<String, Any?>.toCollection() = Collection(
    id = get("col_id") as String,
    name = get("col_name") as String,
    models = (get("col_models_count") as Long?) ?: 0L,
    amount = (get("col_amount_total") as String?)?.let { BigDecimal(it) } ?: BigDecimal.ZERO,
    description = get("col_description") as String?
)

fun Map<String, Any?>.toModel() = Model(
    id = get("mdl_id") as String,
    vehicle = toCar(),
    scale = Scale(get("mdl_scale") as String),
    editor = get("mdl_editor") as String,
    tags = ((get("mdl_tags") as PgArray).array as Array<String>).asList(),
    description = get("mdl_description") as String?
)

fun Map<String, Any?>.toPossessedModel() = PossessedModel(
    model = toModel(),
    boxed = get("mtc_boxed") as Boolean,
    location = get("mtc_location") as String?,
    price = BigDecimal(get("mtc_price") as String),
    buyDate = (get("mtc_buy_date") as Timestamp).toDbString()
)

fun <T> List<T>.toPgArray() = "{${joinToString(",")}}"