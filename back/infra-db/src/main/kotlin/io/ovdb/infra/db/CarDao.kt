package io.ovdb.infra.db

import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarClassification
import io.ovdb.domain.car.CarRepository
import io.ovdb.domain.car.Stats
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service

@Service
class CarDao(internal val jdbcTemplate: NamedParameterJdbcTemplate) : CarRepository {
    override fun find(
        model: String?,
        classification: CarClassification?,
        before: String?,
        after: String?,
        manufacturer: String?
    ): List<Car> {
        var request = """
            select * from cars 
            left join manufacturers m on m.man_id = cars.car_manufacturer
            where car_deleted_date is null
        """

        model?.let { request += " and car_model = :model" }
        classification?.let { request += " and car_classification = :classification" }
        manufacturer?.let { request += " and man_name = :manufacturer" }
        before?.let { request += " and car_fabrication_start_date <= :before" }
        after?.let { request += " and car_fabrication_start_date >= :after" }

        return jdbcTemplate.queryForList(
            request, mapOf(
                "model" to model,
                "classification" to classification?.name,
                "manufacturer" to manufacturer,
                "before" to before?.toTimestamp(),
                "after" to after?.toTimestamp()
            )
        ).map { it.toCar() }
    }

    override fun findById(id: String): Car? = jdbcTemplate.queryForList(
        """
        select * from cars 
        left join manufacturers m on m.man_id = cars.car_manufacturer
        where car_id = :id and car_deleted_date is null 
    """,
        mapOf("id" to id)
    ).firstOrNull()?.toCar()

    override fun create(car: Car) = update(car.copy(id = newId()))

    override fun update(car: Car) = car.apply {
        jdbcTemplate.update(
            """insert into cars(
                    car_id, 
                    car_classification, 
                    car_manufacturer, 
                    car_model, 
                    car_image,
                    car_description,
                    car_fabrication_start_date, 
                    car_fabrication_end_date, 
                    car_created_date, 
                    car_edited_date
                ) values (
                    :id,
                    :classification,
                    :manufacturerId,
                    :model,
                    :image,
                    :description,
                    :fabricationStartDate,
                    :fabricationEndDate,
                    :date,
                    :date
                ) on conflict (car_id) do update set 
                    car_classification = :classification, 
                    car_manufacturer = :manufacturerId, 
                    car_model = :model, 
                    car_image = :image,
                    car_description = :description,
                    car_fabrication_start_date = :fabricationStartDate, 
                    car_fabrication_end_date = :fabricationEndDate, 
                    car_edited_date = :date
            """,
            mapOf(
                "id" to id,
                "classification" to classification.name,
                "manufacturerId" to manufacturer.id,
                "model" to model,
                "image" to image,
                "description" to description,
                "fabricationStartDate" to fabricationStart.toTimestamp(),
                "fabricationEndDate" to fabricationEnd?.toTimestamp(),
                "date" to nowstamp()
            )
        )
    }

    override fun getStats(): Stats  = jdbcTemplate.queryForList("""
        select count(car_id) as total from cars 
    """, emptyMap<String, String>()).first().run {
        Stats(
            total = get("total") as Long
        )
    }

    override fun delete(id: String) {
        jdbcTemplate.update(
            """
            update cars set
                car_deleted_date = :date
            where car_id = :id
        """, mapOf("id" to id, "date" to nowstamp())
        )
    }
}