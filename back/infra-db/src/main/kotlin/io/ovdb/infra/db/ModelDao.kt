package io.ovdb.infra.db

import io.ovdb.domain.models.Model
import io.ovdb.domain.models.ModelRepository
import io.ovdb.domain.models.PossessedModel
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service

@Service
class ModelDao(internal val jdbcTemplate: NamedParameterJdbcTemplate) : ModelRepository {
    override fun find() = jdbcTemplate.queryForList(
        """
           select * from models
           left join cars c on c.car_id = models.mdl_vehicle_id
           left join manufacturers m on c.car_manufacturer = m.man_id
           where mdl_delete_date is null and c.car_deleted_date is null 
        """, emptyMap<String, String>()
    ).map { it.toModel() }

    override fun findById(id: String) = jdbcTemplate.queryForList(
        """
            select * from models
            left join cars c on c.car_id = models.mdl_vehicle_id
            left join manufacturers m on c.car_manufacturer = m.man_id
            where mdl_delete_date is null and c.car_deleted_date is null and mdl_id = :id
        """, mapOf("id" to id)
    ).firstOrNull()?.toModel()

    override fun save(model: Model) = model.run {
        val newId = newId()
        jdbcTemplate.update(
            """
        insert into models (
            mdl_id, 
            mdl_vehicle_id, 
            mdl_scale, 
            mdl_editor, 
            mdl_tags,
            mdl_description, 
            mdl_created_date, 
            mdl_edited_date
        ) values (
            :id,
            :vehicle_id,
            :scale,
            :editor,
            cast(:tags as text[]),
            :description,
            :date,
            :date
        ) on conflict (mdl_id) do update set
            mdl_edited_date = :date,
            mdl_scale = :scale,
            mdl_editor = :editor,
            mdl_tags =cast(:tags as text[]),
            mdl_description = :description,
            mdl_vehicle_id = :vehicle_id
        """,
            mapOf(
                "id" to (id ?: newId),
                "vehicle_id" to vehicle.id,
                "scale" to scale.ratio,
                "editor" to editor,
                "tags" to tags.toPgArray(),
                "description" to description,
                "date" to nowstamp()
            )
        )
        copy(id = id ?: newId)
    }

    override fun delete(id: String) = id.also {
        jdbcTemplate.update("""
            update models
            set mdl_delete_date  = :date
            where mdl_id = :id
        """, mapOf("id" to id, "date" to nowstamp()))
    }

    override fun findInCollection(collectionId: String) = jdbcTemplate.queryForList("""
        select * from models_to_collections
        left join models m on m.mdl_id = models_to_collections.mtc_mdl_id
        left join cars c on m.mdl_vehicle_id = c.car_id
        left join manufacturers m2 on c.car_manufacturer = m2.man_id
        where mtc_col_id = :collectionId
            and mdl_delete_date is null
            and car_deleted_date is null
            and man_deleted_date is null
    """, mapOf("collectionId" to collectionId)).map { it.toPossessedModel() }

    override fun findInCollectionById(collectionId: String, id: String) = jdbcTemplate.queryForList("""
        select * from models_to_collections
        left join models m on m.mdl_id = models_to_collections.mtc_mdl_id
        left join cars c on m.mdl_vehicle_id = c.car_id
        left join manufacturers m2 on c.car_manufacturer = m2.man_id
        where mtc_col_id = :collectionId
            and mtc_mdl_id = :modelId
            and mdl_delete_date is null
            and car_deleted_date is null
            and man_deleted_date is null
    """, mapOf("collectionId" to collectionId, "modelId" to id)).firstOrNull()?.toPossessedModel()

    override fun save(collectionId: String, possessedModel: PossessedModel) = possessedModel.apply {
        jdbcTemplate.update("""
           insert into models_to_collections (
           mtc_mdl_id, 
           mtc_col_id, 
           mtc_boxed, 
           mtc_location, 
           mtc_price, 
           mtc_buy_date, 
           mtc_created_date, 
           mtc_edited_date
        ) values (
            :modelId,
            :collectionId,
            :boxed,
            :location,
            :price,
            :buy_date,
            :date,
            :date
        ) on conflict (mtc_mdl_id, mtc_col_id) do update set
            mtc_buy_date = :buy_date,
            mtc_boxed = :boxed,
            mtc_location = :location,
            mtc_price = :price,
            mtc_edited_date = :date
            
        """, mapOf(
            "modelId" to model.id,
            "collectionId" to collectionId,
            "boxed" to boxed,
            "location" to location,
            "price" to price,
            "buy_date" to buyDate.toTimestamp(),
            "date" to nowstamp()
        ))
    }

    override fun removeFromCollection(possessedModelId: String) = possessedModelId.apply {
        jdbcTemplate.update("""
           delete from models_to_collections where mtc_mdl_id = :modelId
        """, mapOf("modelId" to possessedModelId))
    }
}