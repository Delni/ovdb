package io.ovdb.infra.db

import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarClassification
import org.junit.jupiter.api.*
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.test.context.ContextConfiguration
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isNotNull
import strikt.assertions.isNull

@JdbcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = [CarDao::class, ManufacturerDao::class])
@TestInstance(Lifecycle.PER_CLASS)
internal class CarDaoTest @Autowired constructor(
    private val dao: CarDao,
    val manufacturerDao: ManufacturerDao
) {
    private val table = Table(dao.jdbcTemplate.jdbcTemplate, "cars")

    @BeforeAll
    fun `insert manufacturer`() {
        wipeOutDb(dao.jdbcTemplate.jdbcTemplate)
        manufacturerId = manufacturerDao.create(CAR.manufacturer).id!!
    }

    @AfterEach
    fun `clean table`() {
        table.clean()
    }

    @Test
    fun `should create a car`() {
        dao.create(CAR().copy(id = null))
        expectThat(table).hasNbRows(1)
    }

    @Test
    fun `should create a car with fabrication end date`() {
        dao.create(CAR().copy(id = null, fabricationEnd = "2020-11-10"))
        expectThat(table).hasNbRows(1)
    }

    @Test
    fun `should find by id`() {
        val expected = dao.create(CAR())
        val given = dao.findById(expected.id!!)
        expectThat(given).isNotNull()
        expectThat(given!!) {
            get { model }.isEqualTo(expected.model)
            get { manufacturer }.isEqualTo(expected.manufacturer)
        }
    }

    @Test
    fun `should update car`() {
        val given = dao.create(CAR())

        val updated = dao.update(given.copy(model = "edit"))
        expectThat(updated.model).isEqualTo("edit")
        val secondUpdate = dao.update(given.copy(fabricationEnd = "2020-11-10"))
        expectThat(secondUpdate.fabricationEnd).isEqualTo("2020-11-10")
    }

    @Test
    fun `update should not mutate id`() {
        val result = dao.update(CAR())
        expectThat(result).isEqualTo(CAR())
    }

    @Nested
    inner class FindTest {
        @BeforeEach
        fun `populate database`() {
            table
            val manufacturerId = manufacturerDao.create(Manufacturer(null, "man-test")).id!!
            dao.create(CAR().copy(model = "Yaris"))
            dao.create(CAR().copy(model = "Corolla", manufacturer = Manufacturer(manufacturerId, "man-test")))
            dao.create(CAR().copy(model = "Auris", fabricationStart = "2020-11-11"))
            dao.create(CAR().copy(model = "CR-V", classification = CarClassification.SUV))
        }

        @Test
        fun `should find whole table without parameters`() {
            expectThat(dao.find()) {
                get { size }.isEqualTo(4)
            }
        }

        @Test
        fun `should find by model`() {
            expectThat(dao.find(model="Yaris")) {
                get { size }.isEqualTo(1)
                get { get(0).model }.isEqualTo("Yaris")
            }
        }

        @Test
        fun `should find cars by classification`() {
            expectThat(dao.find(classification = CarClassification.SUV)) {
                get { size }.isEqualTo(1)
                get { get(0).model }.isEqualTo("CR-V")
            }
        }

        @Test
        fun `should find cars by manufacturer name`() {
            expectThat(dao.find(manufacturer = "man-test")) {
                get { size }.isEqualTo(1)
                get { get(0).model }.isEqualTo("Corolla")
            }
        }

        @Test
        fun `should find by fabricationStart`() {
            expectThat(dao.find(before = "2020-11-10")) {
                get { size }.isEqualTo(3)
            }

            expectThat(dao.find(after = "2020-11-10")) {
                get { size }.isEqualTo(1)
            }
        }
    }

    @Test
    fun `should get stats`() {
        dao.create(CAR())
        expectThat(table).hasNbRows(1)
        expectThat(dao.getStats()) {
            get { total }.isEqualTo(1L)
        }
    }

    @Test
    fun delete() {
        val created = dao.create(CAR())
        expectThat(table).hasNbRows(1)
        dao.delete(created.id!!)
        expectThat(table).hasNbRows(1)
        expectThat(dao.findById(created.id!!)).isNull()
    }

    companion object {
        private val CAR = Car(
            id = "id",
            classification = CarClassification.SUBCOMPACT,
            manufacturer = Manufacturer("man_id", "Toyota"),
            model = "test",
            fabricationStart = "2020-11-07",
            description = "description",
            image = null
        )

        private fun CAR() = CAR.copy(manufacturer = CAR.manufacturer.copy(id = manufacturerId))

        var manufacturerId = ""
    }
}