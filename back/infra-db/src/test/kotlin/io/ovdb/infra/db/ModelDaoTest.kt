package io.ovdb.infra.db

import io.ovdb.domain.car.Car
import io.ovdb.domain.car.CarClassification
import io.ovdb.domain.collection.Collection
import io.ovdb.domain.manufacturer.Manufacturer
import io.ovdb.domain.models.Model
import io.ovdb.domain.models.PossessedModel
import io.ovdb.domain.models.Scale
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.test.context.ContextConfiguration
import strikt.api.expectThat
import strikt.assertions.*
import java.math.BigDecimal

@JdbcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = [ModelDao::class, CollectionDao::class, ManufacturerDao::class, CarDao::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ModelDaoTest @Autowired constructor(
    private val dao: ModelDao,
    private val collectionDao: CollectionDao,
    private val manufacturerDao: ManufacturerDao,
    private val carDao: CarDao,
) {
    private val models = Table(dao.jdbcTemplate.jdbcTemplate, "models")
    private val possessedModels = Table(dao.jdbcTemplate.jdbcTemplate, "models_to_collections")

    @BeforeAll
    fun `init database`() {
        wipeOutDb(dao.jdbcTemplate.jdbcTemplate)
        manufacturerDao.update(manufacturer)
        carDao.update(car)
        collectionDao.save(collection)
    }

    @Nested
    inner class Models {
        @AfterEach
        fun clean() {
            models.clean()
        }

        @Test
        fun `should find models`() {
            expectThat(dao.find()) {
                hasSize(0)
            }
        }

        @Test
        fun `should save and update`() {
            val model = save()
            expectThat(models).hasNbRows(1)
            dao.save(model.copy(tags = listOf("Le Mans", "1961")))
            expectThat(models).hasNbRows(1)
            val given = dao.findById(model.id!!)
            expectThat(given)
                .isNotNull()
                .and {
                    get { tags }.contains("Le Mans", "1961")
                }
        }

        @Test
        fun findById() {
            val model = save()
            expectThat(dao.findById(model.id!!))
                .isNotNull()
                .and {
                    get { editor }.isEqualTo("Norev")
                }
        }

        @Test
        fun delete() {
            val model = save()
            dao.delete(model.id!!)
            expectThat(models).hasNbRows(1)
            expectThat(dao.findById(model.id!!)).isNull()
        }

        private fun save(): Model = dao.save(model)
    }

    @Nested
    inner class PossessedModels {
        @AfterEach
        fun clean() {
            possessedModels.clean()
        }

        @Test
        fun findInCollection() {
            save()
            expectThat(dao.findInCollection(collection.id!!)) {
                hasSize(1)
                get { first() }.get { boxed }.isTrue()
            }
        }

        @Test
        fun findInCollectionById() {
            val pModel = save()
            expectThat(dao.findInCollectionById(collection.id!!, pModel.id!!)).isNotNull()
        }

        @Test
        fun `should save and update`() {
            val pModel = save()
            expectThat(possessedModels).hasNbRows(1)
            pModel.copy(price = BigDecimal.TEN)
            dao.save(collection.id!!, pModel)
            expectThat(possessedModels).hasNbRows(1)
        }

        @Test
        fun removeFromCollection() {
            val pModel = save()
            expectThat(possessedModels).hasNbRows(1)
            dao.removeFromCollection(pModel.id!!)
            expectThat(possessedModels).hasNbRows(0)
        }

        private fun save(): PossessedModel = dao.save(model).let {
            dao.save(
                collection.id!!,
                PossessedModel(
                    model = it,
                    boxed = true,
                    location = null,
                    price = BigDecimal.TEN,
                    buyDate = "2020-11-29"
                )
            )
        }
    }

    companion object {
        private val manufacturer = Manufacturer("man_id", "man_name")
        private val car = Car(
            id = "car_id",
            classification = CarClassification.UNKNOWN,
            manufacturer = manufacturer,
            model = "model",
            fabricationStart = "2020-11-28",
            fabricationEnd = null,
            description = null,
            image = null
        )
        private val model = Model(
            id = null,
            vehicle = car,
            description = null,
            scale = Scale.S1_43,
            editor = "Norev",
            tags = emptyList()
        )

        private val collection = Collection(
            id = "col_id",
            name = "col_name",
            description = null
        )


    }


}