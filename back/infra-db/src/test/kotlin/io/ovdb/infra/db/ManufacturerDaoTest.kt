package io.ovdb.infra.db

import io.ovdb.domain.manufacturer.Manufacturer
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.test.context.ContextConfiguration
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isNotNull
import strikt.assertions.isNull

@JdbcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = [ManufacturerDao::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ManufacturerDaoTest @Autowired constructor(
    private val dao: ManufacturerDao
) {

    private val table = Table(dao.jdbcTemplate.jdbcTemplate, "manufacturers")

    @BeforeAll
    fun `clean table`() {
        wipeOutDb(dao.jdbcTemplate.jdbcTemplate)
    }


    @Test
    fun `should create a manufacturer`() {
        dao.create(Manufacturer(id = null, name = "test-manufacturer"))
        expectThat(table).hasNbRows(1)
    }

    @Test
    fun `should find by id`() {
        val expected = dao.create(MANUFACTURER)
        val given = dao.findById(expected.id!!)
        expectThat(given).isNotNull()
        expectThat(given!!) {
            get { name }.isEqualTo (expected.name)
        }
    }

    @Test
    fun `should update manufacturer`() {
        val given = dao.create(MANUFACTURER)

        val updated = dao.update(given.copy(name="edit"))
        expectThat(updated.name).isEqualTo("edit")
    }

    @Test
    fun `update should not mutate id`() {
        val result = dao.update(MANUFACTURER)
        expectThat(result).isEqualTo(MANUFACTURER)
    }

    @Test
    fun delete() {
        val created = dao.create(MANUFACTURER)
        expectThat(table).hasNbRows(1)
        dao.delete(created.id!!)
        expectThat(table).hasNbRows(1)
        expectThat(dao.findById(created.id!!)).isNull()
    }

    @Nested
    inner class FindManufacturerTest {
        @BeforeEach
        fun `populate database`() {
            table.clean()
            dao.create(MANUFACTURER.copy(id = null, name="Audi"))
            dao.create(MANUFACTURER.copy(id = null, name="Toyota"))
            dao.create(MANUFACTURER.copy(id = null, name="Ford"))
        }

        @Test
        fun `should find whole table without parameters`() {
            expectThat(dao.find()) {
                get { size }.isEqualTo(3)
            }
        }

        @Test
        fun `should find by name`() {
            expectThat(dao.find(name="Audi")) {
                get { size }.isEqualTo(1)
                get { get(0).name }.isEqualTo("Audi")
            }
        }
    }

    companion object {
        private val MANUFACTURER = Manufacturer(
            "id",
            "test-manufacturer"
        )
    }
}