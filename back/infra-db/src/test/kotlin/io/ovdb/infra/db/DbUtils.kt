package io.ovdb.infra.db

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.jdbc.JdbcTestUtils
import strikt.api.Assertion

internal data class Table(val jdbcTemplate: JdbcTemplate, val tableName: String) {
    fun clean() {
        jdbcTemplate.update("delete from $tableName")
    }
    override fun toString(): String {
        return "table '$tableName'"
    }
}

internal fun Assertion.Builder<Table>.isEmpty() =
    assert("isEmpty") {
        when (JdbcTestUtils.countRowsInTable(it.jdbcTemplate, it.tableName)) {
            0 -> pass()
            else -> fail(it.jdbcTemplate.queryForList("select * from ${it.tableName}"))
        }
    }

internal fun Assertion.Builder<Table>.hasNbRows(nbRows: Int) =
    assert("has $nbRows rows") {
        when (val count = JdbcTestUtils.countRowsInTable(it.jdbcTemplate, it.tableName)) {
            nbRows -> pass()
            else -> fail("$count => " + it.jdbcTemplate.queryForList("select * from ${it.tableName}"))
        }
    }

internal fun wipeOutDb(jdbcTemplate: JdbcTemplate) {
    listOf(
        "models_to_collections",
        "collections",
        "models",
        "cars",
        "manufacturers"
    ).forEach {
        Table(jdbcTemplate, it).clean()
    }
}