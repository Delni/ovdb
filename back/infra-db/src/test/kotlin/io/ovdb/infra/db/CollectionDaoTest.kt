package io.ovdb.infra.db

import io.ovdb.domain.collection.Collection
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.test.context.ContextConfiguration
import strikt.api.expectThat
import strikt.assertions.*

@JdbcTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = [CollectionDao::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class CollectionDaoTest @Autowired constructor(private val dao: CollectionDao) {

    private val table = Table(dao.jdbcTemplate.jdbcTemplate, "collections")

    @BeforeAll
    fun `clean all`() {
        wipeOutDb(dao.jdbcTemplate.jdbcTemplate)
    }
    @AfterEach
    fun `clean table`() {
        table.clean()
    }

    @Test
    fun `should create a collection`() {
        dao.save(Collection(id = null, name = "test", description = null))
        expectThat(table).hasNbRows(1)
    }

    @Test
    fun `should update a collection`() {
        val collection = dao.save(Collection(id = null, name = "test", description = null))
        expectThat(table).hasNbRows(1)
        dao.save(collection.copy(name = "Altered name"))
        expectThat(table).hasNbRows(1)
    }

    @Test
    fun `should fetch collections`() {
        dao.save(Collection(id = null, name = "test", description = null))
        expectThat(dao.find()) {
            hasSize(1)
            first().get { name }.isEqualTo("test")
        }
    }

    @Test
    fun `should find by  collection's id`() {
        val collection = dao.save(Collection(id = null, name = "test", description = null))
        expectThat(dao.findById(collection.id!!)).isNotNull()
        expectThat(dao.findById("dummy id")).isNull()
    }

    @Test
    fun `should logically delete collection`() {
        val collection = dao.save(Collection(id = null, name = "test", description = null))
        expectThat(dao.findById(collection.id!!)).isNotNull()
        dao.delete(collection.id!!)
        expectThat(dao.findById(collection.id!!)).isNull()
    }
}