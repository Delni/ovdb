package io.ovdb.infra.db

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import strikt.assertions.isNull
import java.math.BigDecimal
import java.sql.Timestamp


class DbModuleTest {
    @Nested
    inner class ToCarTest {

        @Test
        fun `should throw error if any mandatory entry does not exist`() {
            expectThrows<NullPointerException> { emptyMap<String, Any>().toCar() }
            expectThrows<NullPointerException> {
                mapOf(
                    "car_id" to "car_id",
                ).toCar()
            }
            expectThrows<NullPointerException> {
                mapOf(
                    "car_id" to "car_id",
                    "car_classification" to "SUBCOMPACT",
                ).toCar()
            }
            expectThrows<NullPointerException> {
                mapOf(
                    "car_id" to "car_id",
                    "car_classification" to "SUBCOMPACT",
                    "man_id" to "man_id",
                    "man_name" to "man_name"
                ).toCar()
            }
            expectThrows<NullPointerException> {
                mapOf(
                    "car_id" to "car_id",
                    "car_classification" to "SUBCOMPACT",
                    "car_model" to "test-model",
                    "man_id" to "man_id",
                    "man_name" to "man_name"
                ).toCar()
            }
        }

        @Test
        fun `should take a map and return a Car`() {
            expectThat(TEST_MAP.toCar()) {
                get { id }.isEqualTo("car_id")
                get { fabricationStart }.isEqualTo("1970-01-01")
                get { fabricationEnd }.isEqualTo("1970-01-01")
            }
        }

        @Test
        fun `a Car can have no fabrication end date`() {
            val given: Map<String, Any?> = mapOf(
                "car_id" to "test",
                "car_classification" to "SUBCOMPACT",
                "car_model" to "test-model",
                "car_fabrication_start_date" to Timestamp(0L),
                "car_fabrication_end_date" to null,
                "man_id" to "test-man",
                "man_name" to "test-man-n"
            )
            expectThat(given.toCar()) {
                get { fabricationEnd }.isNull()
            }
        }

    }

    @Nested
    inner class ToManufacturerTest {
        @Test
        fun `should throw error if any entry does not exist`() {
            expectThrows<NullPointerException> { emptyMap<String, Any>().toManufacturer() }
            expectThrows<NullPointerException> { mapOf("man_id" to "id").toManufacturer() }
        }

        @Test
        fun `toManufacturer should take a map and return toManufacturer`() {
            expectThat(TEST_MAP.toManufacturer()) {
                get { id }.isEqualTo("man_id")
                get { name }.isEqualTo("man_name")
            }
        }
    }

    @Nested
    inner class ToCollectionTest {
        @Test
        fun `should throw error if id or name does not exist`() {
            expectThrows<NullPointerException> { emptyMap<String, Any>().toCollection() }
            expectThrows<NullPointerException> { mapOf("col_id" to "id").toCollection() }
        }

        @Test
        fun `should return a collection`() {
            val given = mapOf(
                "col_id" to "collectionID",
                "col_name" to "Test Collection"
            )

            expectThat(given.toCollection()) {
                get { id }.isEqualTo("collectionID")
                get { name }.isEqualTo("Test Collection")
            }
        }

        @Test
        fun `should have KPI if exists`() {
            val given = mapOf(
                "col_id" to "collectionID",
                "col_name" to "Test Collection",
                "col_models_count" to 10L,
                "col_amount_total" to "10"
            )

            expectThat(given.toCollection()) {
                get { models }.isEqualTo(10L)
                get { amount }.isEqualTo(BigDecimal.TEN)
            }
        }
    }

    @Test
    fun `should format to PgArray output`() {
        expectThat(emptyList<String>().toPgArray()).isEqualTo("{}")
        expectThat(listOf("item 1", "item 2").toPgArray()).isEqualTo("{item 1,item 2}")
    }

    @Nested
    inner class ToModel {
        @Test
        fun `should throw NPE for mandatory fields`() {
            expectThrows<NullPointerException> { emptyMap<String, Any>().toModel() }
        }
    }

    @Nested
    inner class ToPossessedModel {
        @Test
        fun `should throw NPE for mandatory fields`() {
            expectThrows<NullPointerException> { emptyMap<String, Any>().toPossessedModel() }
        }
    }

    companion object {
        val TEST_MAP = mapOf(
            "car_id" to "car_id",
            "car_classification" to "SUBCOMPACT",
            "car_model" to "test-model",
            "car_fabrication_start_date" to Timestamp(0L),
            "car_fabrication_end_date" to Timestamp(0L),
            "man_id" to "man_id",
            "man_name" to "man_name"
        )
    }
}