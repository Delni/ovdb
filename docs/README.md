# OVDb 🚘
> [![coverage](https://gitlab.com/Delni/ovdb/badges/develop/coverage.svg)](https://delni.gitlab.io/ovdb/?id=tests)
[![pipeline status](https://gitlab.com/Delni/ovdb/badges/develop/pipeline.svg)](https://gitlab.com/Delni/ovdb/-/pipelines)
[![license](https://img.shields.io/badge/license-MIT-success)](https://gitlab.com/Delni/ovdb/-/blob/develop/LICENSE)
[![Kotlin](https://img.shields.io/badge/Kotlin-0095D5?logo=kotlin&labelColor=555)](https://kotlinlang.org)
[![Vue3](https://img.shields.io/badge/-Vue_3-4FC08D?logo=vue.js&labelColor=555)](https://v3.vuejs.org)
[![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?logo=graphql&labelColor=555)](https://graphql.org)  
>Welcome to OVDb, the open vehicle database which also allow you to manage your models collections !

## API
[![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?logo=graphql&labelColor=555)](https://graphql.org)

Playground : [OVDb playground](https://ovdb.herokuapp.com/playground)

## I18N
Currently supported languages : 
- English (soon) 🇬🇧
- Français :fr:

## Tests
* Front ([show results](https://delni.gitlab.io/ovdb/coverage/front))  
    ![Gitlab code coverage (specific job)](https://img.shields.io/gitlab/coverage/Delni/ovdb/develop?job_name=front%3Atest)
    [![Vue3](https://img.shields.io/badge/-Vue_3-4FC08D?logo=vue.js&labelColor=555)](https://v3.vuejs.org)
    [![Jest](https://img.shields.io/badge/-Jest-C21325?logo=jest&labelColor=555)](https://jestjs.io)
    
* Back ([show results](https://delni.gitlab.io/ovdb/coverage/back))  
    ![Gitlab code coverage (specific job)](https://img.shields.io/gitlab/coverage/Delni/ovdb/develop?job_name=back%3Atest)
    [![Kotlin](https://img.shields.io/badge/Kotlin-0095D5?logo=kotlin&labelColor=555)](https://kotlinlang.org)
    [![Strikt](https://img.shields.io/badge/Strikt-0095D5?logo=kotlin&labelColor=555)](https://strikt.io/)
