# OVDb 🚘
> [![coverage](https://gitlab.com/Delni/ovdb/badges/develop/coverage.svg)](https://gitlab.com/Delni/ovdb/develop)
[![pipeline status](https://gitlab.com/Delni/ovdb/badges/develop/pipeline.svg)](https://gitlab.com/Delni/ovdb/-/commits/develop)
[![license](https://img.shields.io/badge/license-MIT-success)](https://gitlab.com/Delni/ovdb/-/blob/develop/LICENSE)
[![Kotlin](https://img.shields.io/badge/Kotlin-0095D5?logo=kotlin&labelColor=555)](https://kotlinlang.org)
[![Vue3](https://img.shields.io/badge/-Vue_3-4FC08D?logo=vue.js&labelColor=555)](https://v3.vuejs.org)
[![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?logo=graphql&labelColor=555)](https://graphql.org)  
>Welcome to OVDb, the open vehicle database which also allow you to manage your models collections !

## API
[![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?logo=graphql&labelColor=555)](https://graphql.org)
:warning: TODO

## Back
[![Kotlin](https://img.shields.io/badge/Kotlin-0095D5?logo=kotlin&labelColor=555)](https://kotlinlang.org)
:warning: TODO


## Front
[![Vue3](https://img.shields.io/badge/-Vue_3-4FC08D?logo=vue.js&labelColor=555)](https://v3.vuejs.org)
:warning: TODO

## I18N 🌐
Currently supported languages : 
- English (soon) 🇬🇧
- Français :fr:


## License 
```MIT License
Copyright (c) 2020 Nicolas Delauney

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```