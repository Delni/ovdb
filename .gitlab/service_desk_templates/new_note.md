## Your issue %{ISSUE_ID} has been updated !

A new comment has been added :
```
%{NOTE_TEXT}
```

As always, if you want to add a comment, simply respond to the email.

Sincerly,
The OVDb team
