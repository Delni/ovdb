stages:
  - compile
  - test
  - package
  - deploy


before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

cache:
  key: ${CI_COMMIT_REF_SLUG}
  policy: push
  paths:
    - build
    - .gradle
    - ui/node_modules

########
# FRONT #
########
front:compile:
  stage: compile
  image: node:14.13.1-alpine3.10
  script:
    - cd ui
    - yarn
    - yarn build
  artifacts:
    paths:
      - ui/dist
    expire_in: 1 hour

front:test:
  stage: test
  image: node:14.13.1-alpine3.10
  script:
    - cd ui
    - yarn
    - yarn test-ci
  coverage: /All\sfiles.*?\s+(\d+.\d+)/
  artifacts:
    paths:
      - ui/coverage/
    reports:
      junit:
        - ./**/**/junit.xml
  needs:
    - front:compile

########
# BACK #
########
back:compile:
  stage: compile
  image: gradle:jdk11
  script:
    - gradle --build-cache assemble

back:test:
  stage: test
  image: gradle:jdk11
  variables:
    POSTGRES_DB: ovdb_test
    POSTGRES_USER: ovdb
    POSTGRES_PASSWORD: ovdb
    POSTGRES_HOST_AUTH_METHOD: trust
    SPRING_PROFILES_ACTIVE: gitlab-ci
  services:
    - name: postgres:latest
  script:
    - gradle check codeCoverageReport --stacktrace
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, "instructions covered"; print 100*covered/instructions, "% covered" }' build/reports/jacoco/codeCoverageReport/codeCoverageReport.csv
  needs:
    - back:compile
  coverage: /(\d+.\d+) \% covered/
  artifacts:
    paths:
      - build/reports/jacoco/codeCoverageReport/html
    reports:
      junit:
        - ./**/**/TEST-*.xml

###########
# PACKAGE #
###########

package:
  stage: package
  image: gradle:jdk11
  script:
    - mv ui/dist/ back/application/src/main/resources/static/
    - gradle assemble
    - mv back/application/build/libs/`ls back/application/build/libs | head -n 1` OpenVehicleDb.jar
  artifacts:
    paths:
      - OpenVehicleDb.jar
    expire_in: 1 week
  needs:
    - front:compile
    - back:compile

heroku:deploy:
  stage: deploy
  image: openjdk:11
  allow_failure: true
  variables:
    HEROKU_APP_NAME: ovdb
  environment:
    name: staging
    url: https://ovdb.herokuapp.com/
  script:
    - curl https://cli-assets.heroku.com/install.sh | sh
    - heroku plugins:install heroku-cli-deploy
    - heroku buildpacks:clear --app $HEROKU_APP_NAME
    - heroku deploy:jar OpenVehicleDb.jar --app $HEROKU_APP_NAME --jdk 11 --options --spring.profiles.active=heroku
  dependencies:
    - package
  needs:
    - package
    - back:test
    - front:test
  only:
    - develop

pages:
  stage: package
  script:
    - mkdir public
    - mkdir -p public/coverage/front
    - mkdir -p public/coverage/back
    - cp -r docs/* public
    - cp -r ui/coverage/* public/coverage/front/
    - cp -r build/reports/jacoco/codeCoverageReport/html/* public/coverage/back/
  needs:
    - front:test
    - back:test
  artifacts:
    paths:
      - public
  only:
    - develop
